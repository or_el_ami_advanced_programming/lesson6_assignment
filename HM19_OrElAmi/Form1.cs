﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace HM19_OrElAmi
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string[] users = System.IO.File.ReadAllLines("Users.txt");
            string log = textBox1.Text + "," + textBox2.Text;
            foreach(string user in users)
                if(user.Equals(log))
                {
                    Form2 f = new Form2();
                    f.Username = textBox1.Text;
                    Hide();
                    f.ShowDialog();
                    Close();
                }
            MessageBox.Show("Wrong Password! please try again...");
        }
        
        private void textBox1_Click(object sender, EventArgs e)
        {
            textBox1.Text = (textBox1.Text == "username") ? "" : textBox1.Text;
        }

        private void textBox2_Click(object sender, EventArgs e)
        {
            textBox2.Text = (textBox2.Text == "password") ? "" : textBox2.Text;
        }
    }
}
