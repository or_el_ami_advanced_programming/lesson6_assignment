﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HM19_OrElAmi
{
    public partial class Form2 : Form
    {
        private string _userName;
        private Dictionary<DateTime, string> _dates = new Dictionary<DateTime, string>();


        public Form2()
        {
            InitializeComponent();
        }

        public string Username
        {
            get { return _userName; }
            set { _userName = value; label1.Text = "Hello " + value; }
        }

        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
            textBox1.Text = "On the selected date - ";
            if (_dates.ContainsKey(monthCalendar1.SelectionStart))
                textBox1.Text += _dates[monthCalendar1.SelectionStart];
            else textBox1.Text += "no one";
            textBox1.Text += " has a birthday!";
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            string[] date;
            if (!File.Exists(_userName + "BD.txt"))
            {
                File.Create(_userName + "BD.txt");
            }
            else
            {
                string[] dates = System.IO.File.ReadAllLines(_userName + "BD.txt");
                for (int i = 0; i < dates.Length; i++)
                {
                    date = dates[i].Split(',')[1].Split('/');
                    _dates.Add(new DateTime(int.Parse(date[2]), int.Parse(date[0]), int.Parse(date[1])), dates[i].Split(',')[0]);
                }
            }
            textBox1.Text = "On the selected date - ";
            if (_dates.ContainsKey(monthCalendar1.TodayDate))
                textBox1.Text += _dates[monthCalendar1.TodayDate];
            else textBox1.Text += "no one";
            textBox1.Text += " has a birthday!";
        }
    }
}
